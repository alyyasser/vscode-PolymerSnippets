# Polymer Snippets for VSCode

[Polymer Snippets for VSCode][marketplace-url] based on [Atom Polymer Snippets][atom-polymer-url] and [Sublime Polymer Snippets][sublime-polymer-url] by [Rob Dodson][rob-dodson-url].

## Installation

1. Visit [VSCode Market: Polymer Snippets for VSCode][marketplace-url].<br><br>**OR**

2. By VSCode
   * Open VSCode
   * Launch VS Code Quick Open (press **Ctrl+P**) 
   * Paste the following command

     ```
     ext install vscode-polymer-snippets
     ```
   * Press **Enter** to search the extension
   * Select **Polymer Snippets** 
   * Install

## Usage

Type snippet prefix, and IntelliSense will show the snippet. If IntelliSense doesn't show the snippet, press **Ctrl+Space** and then **Enter** to insert the snippet.

Snippet Name | Prefix | Description
--- | --- | ---
[html-import-app-elements][html-import-app-elements-src] | `hi-app` | HTML Import: app-elements
[html-import-app-layout][html-import-app-layout-src] | `hi-app-layout` | HTML Import: app-layout
[html-import-app-localize][html-import-app-localize-src] | `hi-app-localize` | HTML Import: app-localize-behavior
[html-import-app-pouchdb][html-import-app-pouchdb-src] | `hi-app-pouchdb` | HTML Import: app-puchdb-*
[html-import-app-route][html-import-app-route-src] | `hi-app-route` | HTML Import: app-route
[html-import-app-location][html-import-app-location-src] | `hi-app-location` | HTML Import: app-location
[html-import-app-route-converter][html-import-app-route-converter-src] | `hi-app-route-converter` | HTML Import: app-route-converter
[html-import-app-localstorage-document][html-import-app-localstorage-document-src] | `hi-app-localstorage` | HTML Import: app-localstorage-document
[html-import-app-indexeddb-mirror][html-import-app-indexeddb-mirror-src] | `hi-app-indexeddb` | HTML Import: app-indexeddb-mirror
[html-import-iron-elements][html-import-iron-elements-src] | `hi-fe` | HTML Import: iron-elements
[html-import-iron-a11y-announcer][html-import-iron-a11y-announcer-src] | `hi-fe-a11y-announcer` | HTML Import: iron-a11y-announcer
[html-import-a11y-keys][html-import-a11y-keys-src] | `hi-fe-a11y-keys` | HTML Import: iron-a11y-keys
[html-import-iron-ajax][html-import-iron-ajax-src] | `hi-fe-ajax` | HTML Import: iron-ajax
[html-import-iron-autogrow-textarea][html-import-iron-autogrow-textarea-src] | `hi-fe-textarea` | HTML Import: iron-autogrow-textarea
[html-import-iron-button-state][html-import-iron-button-state-src] | `hi-fe-button-state` | HTML Import: iron-behaviors -> iron-button-state
[html-import-iron-checked-element-behavior][html-import-iron-checked-element-behavior-src] | `hi-fe-checked-element` | HTML Import: iron-checked-element-behavior
[html-import-iron-collapse][html-import-iron-collapse-src] | `hi-fe-collapse` | HTML Import: iron-collapse
[html-import-iron-component-page][html-import-iron-component-page-src] | `hi-fe-component` | HTML Import: iron-component-page
[html-import-iron-demo-snippet][html-import-iron-demo-snippet-src] | `hi-fe-demo` | HTML Import: iron-demo-helpers -> demo-snippet
[html-import-iron-doc-viewer][html-import-iron-doc-viewer-src] | `hi-fe-doc` | HTML Import: iron-doc-viewer
[html-import-iron-dropdown][html-import-iron-dropdown-src] | `hi-fe-dropdown` | HTML Import: iron-dropdown
[html-import-iron-fit-behavior][html-import-iron-fit-behavior-src] | `hi-fe-fit` | HTML Import: iron-fit-behavior
[html-import-iron-flex-layout][html-import-iron-flex-layout-src] | `hi-fe-flex` | HTML Import: iron-flex-layout
[html-import-iron-flex-layout-classes][html-import-iron-flex-layout-classes-src] | `hi-fe-flex-classes` | HTML Import: iron-flex-layout-classes
[html-import-iron-form][html-import-iron-form-src] | `hi-fe-form` | HTML Import: iron-form
[html-import-iron-form-element-behavior][html-import-iron-form-element-behavior-src] | `hi-fe-form-element` | HTML Import: iron-form-element-behavior
[html-import-iron-icon][html-import-iron-icon-src] | `hi-fe-ic` | HTML Import: iron-icon
[html-import-iron-icons][html-import-iron-icons-src] | `hi-fe-ics` | HTML Import: iron-icons
[html-import-iron-icons-av][html-import-iron-icons-av-src] | `hi-fe-ics-av` | HTML Import: av-icons
[html-import-iron-icons-communication][html-import-iron-icons-communication-src] | `hi-fe-ics-communication` | HTML Import: communication-icons
[html-import-iron-icons-device][html-import-iron-icons-device-src] | `hi-fe-ics-device` | HTML Import: device-icons
[html-import-iron-icons-editor][html-import-iron-icons-editor-src] | `hi-fe-ics-editor` | HTML Import: editor-icons
[html-import-iron-icons-hardware][html-import-iron-icons-hardware-src] | `hi-fe-ics-hardware` | HTML Import: hardware-icons
[html-import-iron-icons-image][html-import-iron-icons-image-src] | `hi-fe-ics-image` | HTML Import: image-icons
[html-import-iron-icons-maps][html-import-iron-icons-maps-src] | `hi-fe-ics-maps` | HTML Import: maps-icons
[html-import-iron-icons-notification][html-import-iron-icons-notification-src] | `hi-fe-ics-notification` | HTML Import: notification-icons
[html-import-iron-icons-places][html-import-iron-icons-places-src] | `hi-fe-ics-places` | HTML Import: places-icons
[html-import-iron-icons-social][html-import-iron-icons-social-src] | `hi-fe-ics-social` | HTML Import: social-icons
[html-import-iron-icons-update][html-import-iron-icons-update-src] | `hi-fe-ics-update` | HTML Import: update-icons
[html-import-iron-iconset][html-import-iron-iconset-src] | `hi-fe-icset` | HTML Import: iron-iconset
[html-import-iron-iconset-svg][html-import-iron-iconset-svg-src] | `hi-fe-icset-svg` | HTML Import: iron-iconset-svg
[html-import-iron-image][html-import-iron-image-src] | `hi-fe-image` | HTML Import: iron-image
[html-import-iron-input][html-import-iron-input-src] | `hi-fe-input` | HTML Import: iron-input
[html-import-iron-jsonp-library][html-import-iron-jsonp-library-src] | `hi-fe-jsonp` | HTML Import: iron-jsonp-library
[html-import-iron-label][html-import-iron-label-src] | `hi-fe-label` | HTML Import: iron-label
[html-import-iron-list][html-import-iron-list-src] | `hi-fe-list` | HTML Import: iron-list
[html-import-iron-localstorage][html-import-iron-localstorage-src] | `hi-fe-local` | HTML Import: iron-localstorage
[html-import-iron-location][html-import-iron-location-src] | `hi-fe-location` | HTML Import: iron-location
[html-import-iron-media-query][html-import-iron-media-query-src] | `hi-fe-media-query` | HTML Import: iron-media-query
[html-import-iron-menu-behavior][html-import-iron-menu-behavior-src] | `hi-fe-menu` | HTML Import: iron-menu-behavior
[html-import-iron-menubar-behavior][html-import-iron-menubar-behavior-src] | `hi-fe-menubar` | HTML Import: iron-menubar-behavior
[html-import-iron-meta][html-import-iron-meta-src] | `hi-fe-meta` | HTML Import: iron-meta
[html-import-iron-overlay-behavior][html-import-iron-overlay-behavior-src] | `hi-fe-overlay` | HTML Import: iron-overlay-behavior
[html-import-iron-pages][html-import-iron-pages-src] | `hi-fe-pages` | HTML Import: iron-pages
[html-import-iron-range-behavior][html-import-iron-range-behavior-src] | `hi-fe-range` | HTML Import: iron-range-behavior
[html-import-iron-resizable-behavior][html-import-iron-resizable-behavior-src] | `hi-fe-resize` | HTML Import: iron-resizable-behavior
[html-import-iron-scroll-target-behavior][html-import-iron-scroll-target-behavior-src] | `hi-fe-scroll-target` | HTML Import: iron-scroll-target-behavior
[html-import-iron-scroll-threshold][html-import-iron-scroll-threshold-src] | `hi-fe-scroll-threshold` | HTML Import: iron-scroll-threshold
[html-import-iron-selector][html-import-iron-selector-src] | `hi-fe-selector` | HTML Import: iron-selector
[html-import-iron-signals][html-import-iron-signals-src] | `hi-fe-signals` | HTML Import: iron-signals
[html-import-iron-swipeable-container][html-import-iron-swipeable-container-src] | `hi-fe-swipeable` | HTML Import: iron-swipeable-container
[html-import-iron-validatable-behavior][html-import-iron-validatable-behavior-src] | `hi-fe-validatable` | HTML Import: iron-validatable-behavior
[html-import-iron-validator-behavior][html-import-iron-validator-behavior-src] | `hi-fe-validator` | HTML Import: iron-validator-behavior
[html-import-google-web-components][html-import-google-web-components-src] | `hi-go` | HTML Import: google-web-components
[html-import-google-analytics][html-import-google-analytics-src] | `hi-go-analytics` | HTML Import: google-analytics
[html-import-google-apis][html-import-google-apis-src] | `hi-go-apis` | HTML Import: google-apis
[html-import-google-client-loader][html-import-google-client-loader-src] | `hi-go-client` | HTML Import: google-client-loader
[html-import-google-maps-api][html-import-google-maps-api-src] | `hi-go-api-maps` | HTML Import: google-maps-api
[html-import-google-plusone-api][html-import-google-plusone-api-src] | `hi-go-api-plusone` | HTML Import: google-plusone-api
[html-import-google-realtime-api][html-import-google-realtime-api-src] | `hi-go-api-realtime` | HTML Import: google-realtime-api
[html-import-google-youtube-api][html-import-google-youtube-api-src] | `hi-go-api-youtube` | HTML Import: google-youtube-api
[html-import-google-js-api][html-import-google-js-api-src] | `hi-go-api-js` | HTML Import: google-js-api
[html-import-google-calendar][html-import-google-calendar-src] | `hi-go-calendar` | HTML Import: google-calendar
[html-import-google-castable-video][html-import-google-castable-video-src] | `hi-go-video` | HTML Import: google-castable-video
[html-import-google-chart][html-import-google-chart-src] | `hi-go-chart` | HTML Import: google-chart
[html-import-google-feeds][html-import-google-feeds-src] | `hi-go-feeds` | HTML Import: google-feeds
[html-import-google-hangout-button][html-import-google-hangout-button-src] | `hi-go-hangout` | HTML Import: google-hangout-button
[html-import-google-map-elements][html-import-google-map-elements-src] | `hi-go-map` | HTML Import: google-map-elements
[html-import-google-sheets][html-import-google-sheets-src] | `hi-go-sheets` | HTML Import: google-sheets
[html-import-google-signin][html-import-google-signin-src] | `hi-go-signin` | HTML Import: google-signin
[html-import-google-signin-aware][html-import-google-signin-aware-src] | `hi-go-signin-aware` | HTML Import: google-signin-aware
[html-import-google-streetview-pano][html-import-google-streetview-pano-src] | `hi-go-streetview-pano` | HTML Import: google-streetview-pano
[html-import-google-url-shortener][html-import-google-url-shortener-src] | `hi-go-shortener` | HTML Import: google-url-shortener
[html-import-google-youtube][html-import-google-youtube-src] | `hi-go-youtube` | HTML Import: google-youtube
[html-import-google-youtube-upload][html-import-google-youtube-upload-src] | `hi-go-youtube-upload` | HTML Import: google-youtube-upload
[html-import-polymerfire][html-import-polymerfire-src] | `hi-polymerfire` | HTML Import: polymerfire
[html-import-paper-elements][html-import-paper-elements-src] | `hi-md` | HTML Import: paper-elements
[html-import-paper-badge][html-import-paper-badge-src] | `hi-md-badge` | HTML Import: paper-badge
[html-import-paper-button-behavior][html-import-paper-button-behavior-src] | `hi-md-button-behavior` | HTML Import: paper-button-behavior
[html-import-paper-checked-element-behavior][html-import-paper-checked-element-behavior-src] | `hi-md-checked-behavior` | HTML Import: paper-checked-element-behavior
[html-import-paper-inky-focus-behavior][html-import-paper-inky-focus-behavior-src] | `hi-md-focus-behavior` | HTML Import: paper-inky-focus-behavior
[html-import-paper-ripple-behavior][html-import-paper-ripple-behavior-src] | `hi-md-ripple-behavior` | HTML Import: paper-ripple-behavior
[html-import-paper-button][html-import-paper-button-src] | `hi-md-button` | HTML Import: paper-button
[html-import-paper-card][html-import-paper-card-src] | `hi-md-card` | HTML Import: paper-card
[html-import-paper-checkbox][html-import-paper-checkbox-src] | `hi-md-checkbox` | HTML Import: paper-checkbox
[html-import-paper-dialog][html-import-paper-dialog-src] | `hi-md-dialog` | HTML Import: paper-dialog
[html-import-paper-dialog-behavior][html-import-paper-dialog-behavior-src] | `hi-md-dialog-behavior` | HTML Import: paper-dialog-behavior
[html-import-paper-dialog-shared-styles][html-import-paper-dialog-shared-styles-src] | `hi-md-dialog-styles` | HTML Import: paper-dialog-shared-styles
[html-import-paper-dialog-scrollable][html-import-paper-dialog-scrollable-src] | `hi-md-dialog-scrollable` | HTML Import: paper-dialog-scrollable
[html-import-paper-drawer-panel][html-import-paper-drawer-panel-src] | `hi-md-drawer` | HTML Import: paper-drawer-panel
[html-import-paper-dropdown-menu][html-import-paper-dropdown-menu-src] | `hi-md-dropdown` | HTML Import: paper-dropdown-menu
[html-import-paper-dropdown-menu-light][html-import-paper-dropdown-menu-light-src] | `hi-md-dropdown-light` | HTML Import: paper-dropdown-menu-light
[html-import-paper-fab][html-import-paper-fab-src] | `hi-md-fab` | HTML Import: paper-fab
[html-import-paper-header-panel][html-import-paper-header-panel-src] | `hi-md-header` | HTML Import: paper-header-panel
[html-import-paper-icon-button][html-import-paper-icon-button-src] | `hi-md-icon-button` | HTML Import: paper-icon-button
[html-import-paper-icon-button-light][html-import-paper-icon-button-light-src] | `hi-md-icon-button-light` | HTML Import: paper-icon-button-light
[html-import-paper-input][html-import-paper-input-src] | `hi-md-input` | HTML Import: paper-input
[html-import-paper-textarea][html-import-paper-textarea-src] | `hi-md-textarea` | HTML Import: paper-textarea
[html-import-paper-item][html-import-paper-item-src] | `hi-md-item` | HTML Import: paper-item
[html-import-paper-item-body][html-import-paper-item-body-src] | `hi-md-item-body` | HTML Import: paper-item-body
[html-import-paper-icon-item][html-import-paper-icon-item-src] | `hi-md-icon-item` | HTML Import: paper-icon-item
[html-import-paper-listbox][html-import-paper-listbox-src] | `hi-md-listbox` | HTML Import: paper-listbox
[html-import-paper-material][html-import-paper-material-src] | `hi-md-material` | HTML Import: paper-material
[html-import-paper-menu][html-import-paper-menu-src] | `hi-md-menu` | HTML Import: paper-menu
[html-import-paper-submenu][html-import-paper-submenu-src] | `hi-md-submenu` | HTML Import: paper-submenu
[html-import-paper-menu-button][html-import-paper-menu-button-src] | `hi-md-menu-button` | HTML Import: paper-menu-button
[html-import-paper-progress][html-import-paper-progress-src] | `hi-md-progress` | HTML Import: paper-progress
[html-import-paper-radio-button][html-import-paper-radio-button-src] | `hi-md-radio-button` | HTML Import: paper-radio-button
[html-import-paper-ripple][html-import-paper-ripple-src] | `hi-md-ripple` | HTML Import: paper-ripple
[html-import-paper-scroll-header-panel][html-import-paper-scroll-header-panel-src] | `hi-md-scroll-header` | HTML Import: paper-scroll-header-panel
[html-import-paper-slider][html-import-paper-slider-src] | `hi-md-slider` | HTML Import: paper-slider
[html-import-paper-spinner][html-import-paper-spinner-src] | `hi-md-spinner` | HTML Import: paper-spinner
[html-import-paper-spinner-lite][html-import-paper-spinner-lite-src] | `hi-md-spinner-lite` | HTML Import: paper-spinner-lite
[html-import-paper-styles-color][html-import-paper-styles-color-src] | `hi-md-color` | HTML Import: paper-styles -> color
[html-import-paper-styles-default-theme][html-import-paper-styles-default-theme-src] | `hi-md-theme` | HTML Import: paper-styles -> default-theme
[html-import-paper-styles-shadow][html-import-paper-styles-shadow-src] | `hi-md-shadow` | HTML Import: paper-styles -> shadow
[html-import-paper-styles-typography][html-import-paper-styles-typography-src] | `hi-md-typography` | HTML Import: paper-styles -> typography
[html-import-paper-styles-demo-pages][html-import-paper-styles-demo-pages-src] | `hi-md-demo-pages` | HTML Import: paper-styles -> demo-pages
[html-import-paper-tabs][html-import-paper-tabs-src] | `hi-md-tabs` | HTML Import: paper-tabs
[html-import-paper-toast][html-import-paper-toast-src] | `hi-md-toast` | HTML Import: paper-toast
[html-import-paper-toggle-button][html-import-paper-toggle-button-src] | `hi-md-toggle` | HTML Import: paper-toggle-button
[html-import-paper-toolbar][html-import-paper-toolbar-src] | `hi-md-toolbar` | HTML Import: paper-toolbar
[html-import-paper-tooltip][html-import-paper-tooltip-src] | `hi-md-tooltip` | HTML Import: paper-tooltip
[html-import-neon-animation][html-import-neon-animation-src] | `hi-ne` | HTML Import: neon-animation
[html-import][html-import-src] | `hi` | HTML Import
[html-import-polymer][html-import-polymer-src] | `hi-polymer` | HTML Import: Polymer
[html-import-shared-styles][html-import-shared-styles-src] | `hi-styles` | HTML Import: Shared Styles
[html-template-default-with-polyfill][html-template-default-with-polyfill-src] | `ht` | HTML Template: with Web Components polyfill
[html-template-with-polyfill-when-not-supported][html-template-with-polyfill-when-not-supported-src] | `ht-poly` | HTML Template: with Web Components polyfill loaded when not supported by browser

## License

[Polymer Snippets for VSCode License][license-url] © Aly Yasser Pranajaya

[Atom Polymer Snippets License][rob-dodson-license-url] © Rob Dodson

[Sublime Polymer Snippets License][rob-dodson-license-url] © Rob Dodson

[marketplace-url]: https://marketplace.visualstudio.com/items?itemName=NgekNgok.vscode-polymer-snippets
[readme-url]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/blob/master/README.md
[license-url]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/blob/master/LICENSE

[atom-polymer-url]: https://github.com/robdodson/Atom-PolymerSnippets
[sublime-polymer-url]: https://github.com/robdodson/PolymerSnippets
[rob-dodson-url]: https://github.com/robdodson
[rob-dodson-license-url]: http://robdodson.mit-license.org/

[html-import-app-elements-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-app.json#L2
[html-import-app-layout-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-app.json#L7
[html-import-app-localize-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-app.json#L12
[html-import-app-pouchdb-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-app.json#L17
[html-import-app-route-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-app.json#L22
[html-import-app-location-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-app.json#L27
[html-import-app-route-converter-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-app.json#L32
[html-import-app-localstorage-document-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-app.json#L37
[html-import-app-indexeddb-mirror-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-app.json#L42
[html-import-iron-elements-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L2
[html-import-iron-a11y-announcer-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L7
[html-import-a11y-keys-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L12
[html-import-iron-ajax-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L17
[html-import-iron-autogrow-textarea-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L22
[html-import-iron-button-state-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L27
[html-import-iron-checked-element-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L32
[html-import-iron-collapse-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L37
[html-import-iron-component-page-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L42
[html-import-iron-demo-snippet-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L47
[html-import-iron-doc-viewer-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L52
[html-import-iron-dropdown-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L57
[html-import-iron-fit-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L62
[html-import-iron-flex-layout-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L67
[html-import-iron-flex-layout-classes-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L72
[html-import-iron-form-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L77
[html-import-iron-form-element-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L82
[html-import-iron-icon-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L87
[html-import-iron-icons-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L92
[html-import-iron-icons-av-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L97
[html-import-iron-icons-communication-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L102
[html-import-iron-icons-device-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L107
[html-import-iron-icons-editor-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L112
[html-import-iron-icons-hardware-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L117
[html-import-iron-icons-image-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L122
[html-import-iron-icons-maps-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L127
[html-import-iron-icons-notification-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L132
[html-import-iron-icons-places-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L137
[html-import-iron-icons-social-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L142
[html-import-iron-icons-update-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L147
[html-import-iron-iconset-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L152
[html-import-iron-iconset-svg-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L157
[html-import-iron-image-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L162
[html-import-iron-input-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L167
[html-import-iron-jsonp-library-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L172
[html-import-iron-label-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L177
[html-import-iron-list-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L182
[html-import-iron-localstorage-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L187
[html-import-iron-location-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L192
[html-import-iron-media-query-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L197
[html-import-iron-menu-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L202
[html-import-iron-menubar-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L207
[html-import-iron-meta-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L212
[html-import-iron-overlay-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L217
[html-import-iron-pages-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L222
[html-import-iron-range-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L227
[html-import-iron-resizable-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L232
[html-import-iron-scroll-target-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L237
[html-import-iron-scroll-threshold-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L242
[html-import-iron-selector-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L247
[html-import-iron-signals-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L252
[html-import-iron-swipeable-container-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L257
[html-import-iron-validatable-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L262
[html-import-iron-validator-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-fe.json#L267
[html-import-google-web-components-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L2
[html-import-google-analytics-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L7
[html-import-google-apis-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L12
[html-import-google-client-loader-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L17
[html-import-google-maps-api-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L22
[html-import-google-plusone-api-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L27
[html-import-google-realtime-api-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L32
[html-import-google-youtube-api-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L37
[html-import-google-js-api-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L42
[html-import-google-calendar-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L47
[html-import-google-castable-video-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L52
[html-import-google-chart-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L57
[html-import-google-feeds-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L62
[html-import-google-hangout-button-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L67
[html-import-google-map-elements-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L72
[html-import-google-sheets-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L77
[html-import-google-signin-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L82
[html-import-google-signin-aware-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L87
[html-import-google-streetview-pano-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L92
[html-import-google-url-shortener-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L97
[html-import-google-youtube-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L102
[html-import-google-youtube-upload-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L107
[html-import-polymerfire-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-go.json#L112
[html-import-paper-elements-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L2
[html-import-paper-badge-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L7
[html-import-paper-button-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L12
[html-import-paper-checked-element-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L17
[html-import-paper-inky-focus-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L22
[html-import-paper-ripple-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L27
[html-import-paper-button-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L32
[html-import-paper-card-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L37
[html-import-paper-checkbox-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L42
[html-import-paper-dialog-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L47
[html-import-paper-dialog-behavior-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L52
[html-import-paper-dialog-shared-styles-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L57
[html-import-paper-dialog-scrollable-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L62
[html-import-paper-drawer-panel-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L67
[html-import-paper-dropdown-menu-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L72
[html-import-paper-dropdown-menu-light-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L77
[html-import-paper-fab-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L82
[html-import-paper-header-panel-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L87
[html-import-paper-icon-button-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L92
[html-import-paper-icon-button-light-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L97
[html-import-paper-input-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L102
[html-import-paper-textarea-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L107
[html-import-paper-item-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L112
[html-import-paper-item-body-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L117
[html-import-paper-icon-item-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L122
[html-import-paper-listbox-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L127
[html-import-paper-material-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L132
[html-import-paper-menu-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L137
[html-import-paper-submenu-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L142
[html-import-paper-menu-button-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L147
[html-import-paper-progress-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L152
[html-import-paper-radio-button-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L157
[html-import-paper-ripple-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L162
[html-import-paper-scroll-header-panel-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L167
[html-import-paper-slider-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L172
[html-import-paper-spinner-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L177
[html-import-paper-spinner-lite-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L182
[html-import-paper-styles-color-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L187
[html-import-paper-styles-default-theme-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L192
[html-import-paper-styles-shadow-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L197
[html-import-paper-styles-typography-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L202
[html-import-paper-styles-demo-pages-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L207
[html-import-paper-tabs-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L212
[html-import-paper-toast-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L217
[html-import-paper-toggle-button-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L222
[html-import-paper-toolbar-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L227
[html-import-paper-tooltip-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-md.json#L232
[html-import-neon-animation-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import-ne.json#L2
[html-import-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import.json#L2
[html-import-polymer-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import.json#L7
[html-import-shared-styles-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-import.json#L12
[html-template-default-with-polyfill-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-template.json#L2
[html-template-with-polyfill-when-not-supported-src]: https://gitlab.com/alyyasser/vscode-PolymerSnippets/tree/master/snippets/html-template.json#L39